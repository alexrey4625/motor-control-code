//////////////////////////////////////////////////////////////////////
// BLDC Motor Control for E-bike, Currently Prototyping on RC Motor
// Senior Project for EECE 473 at Western Washington University
// Daichi Tamai, Issam Khouri, Joel Perez, & Reyes Navarro
// Version Date: 5/18/21
//
//////////////////////////////////////////////////////////////////////
//#include <avr/io.h>

// These variables store the values of the hall sensor readings (rotor position)
int sector;
byte logic;

//const byte riseDetect = 2;
volatile byte state = 0; 

// These variables are to used to calculate the speed/frequency of the motor
float time1;
float time2;
int counter;
float timeVal; //stores difference between time1 and time2
float freq; //stores final calculated frequency value, used to output to serial monitor

//PWM pin configuration

//int analogPin = 3; //P Pot to analog pin 3
int duty = 175; // duty cycle of upper PWM pin outputs.
int ON = 255;
int pwmprescaler = 0x02; //Adjust PWM frequecy output, Used on timers TCCR3B & TCCR4B

void setup() {
  Serial.begin(9600);

  //pinMode(riseDetect, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(riseDetect), blink, RISING);

  //Set GPIO
  DDRE |= _BV(PE4) | _BV(PE5) | _BV(PE3); //sets PIN2,3,5
  //Default low
  PORTE &= ~(_BV(PE4) | _BV(PE5) | _BV(PE3));

  //Set GPIO
  DDRH |= _BV(PH3) | _BV(PH4) | _BV(PH5); //sets PIN6,7,8
  //Default low
  PORTH &= ~(_BV(PH3) | _BV(PH4) | _BV(PH5));

  TCCR3B &= ~(_BV(CS32) | _BV(CS30)); // Clear prior settings from Arduino.
  TCCR4B &= ~(_BV(CS42) | _BV(CS40)); // Clear prior settings from Arduino.

  // Interrupt enabling for port B 0-7
  PCICR = PCICR & 0b00000000 | 1;            //enable interrupts for pins 0-7
  PCMSK0 = PCMSK0 & 0b00000000 | 0x70;        //Mask for enabling pins 4,5,6

  // Initial call of sensor and motor functions
    sensor_Read();        //read hall sensors
    motor_Move();         //move motor
}

void loop() {

  // Currently commented out below is the frequency calculation code, Currently running open-loop system

  //    duty=((analogRead(val)/4));
  //  Serial.print("Duty Cycle Value: ");
  //  Serial.print(duty);
  //  Serial.print("\n");
  //
  //  if (counter == 1) {
  //    time1 = millis();
  //  }
  //  else if (counter == 3) {
  //    counter = 1;
  //    time2 = millis();
  //    timeVal = (time2 - time1);
  //    freq = (1 / (timeVal / 1000));
  //    Serial.print(timeVal);
  //    Serial.print(", ");
  //    Serial.print(freq);
  //    Serial.println();
  //  }
  //  else {}
  //
  //  Serial.println();
  //  Serial.print(freq);
  //  Serial.println();


}
////////////////////////////////////////////////////////////////////////////////////////////
// motor_Move Function
// Updated date: 4/19/21
// This function first determines from the rotor position (hall sensor readings). This info
// is used to update the sector variable. Then using a switch statement the appropiate PWM
// signals, the ones that correspond to what phase needs to be excited in oder to continue
// rotation of the motor, are output.
//
////////////////////////////////////////////////////////////////////////////////////////////
void motor_Move() {
  
  TCCR3B &= ~(_BV(CS32) | _BV(CS30)); // Clear prior settings from Arduino.
  TCCR4B &= ~(_BV(CS42) | _BV(CS40)); // Clear prior settings from Arduino.
//sector = 1;
  if (logic == 5) { //101
    sector = 1;
  }
  else if (logic == 4) { //100
    sector = 2;
  }
  else if (logic == 6) { //110
    sector = 3;
  }
  else if (logic == 2) { //010
    sector = 4;
  }
  else if (logic == 3) {//011
    sector = 5;
  }
  else if (logic == 1) {//001
    sector = 6;
  }
  else {}
  switch (sector) {
    case 1:
      //PWM output for sector 1
      // Turn on q1 and q4 (0-60 degrees), all other pins are turned off to prevent short

      TCCR4A =  0; //Clear timers
      TCCR3A =  0; //Clear timers

//      //Q1 Q4
      OCR3B = duty; //sets duty (PIN2)
      TCCR3A |=  (0x30); //non-inverting
      TCCR3A |=  (1 << WGM30);
      TCCR3B |=  (1 >> CS31);
      TCCR3B |=  (1 >> WGM32);

      OCR4A = ON; //sets duty (PIN5)
      TCCR4A |=  (0x80); //non-inverting
      TCCR4A |=  (1 << WGM40);
      TCCR4B |=  (1 >> CS40);
      TCCR4B |=  (1 >> WGM42);

      break;
    case 2:
      //PWM output for sector 2
      // Turn on q1 and q6 (60-120 degrees), all other pins are turned off to prevent short

      TCCR4A =  0; //Clear timers
      TCCR3A =  0; //Clear timers

      //Q1 Q6
      OCR3B = duty; //sets duty (PIN2)
      TCCR3A |=  (0x30); //non-inverting
      TCCR3A |=  (1 << WGM30);
      TCCR3B |=  (1 >> CS31);
      TCCR3B |=  (1 >> WGM32);

      OCR4A = ON; //sets duty (PIN8)
      TCCR4A |=  (0x0C); //non-inverting
      TCCR4A |=  (1 << WGM40);
      TCCR4B |=  (1 >> CS40);
      TCCR4B |=  (1 >> WGM42);

      break;
    case 3:
      //PWM output for sector 3
      // Turn on q3 and q6 (120-180 degrees), all other pins are turned off to prevent short

      TCCR4A =  0; //Clear timers
      TCCR3A =  0; //Clear timers

      //Q3 Q6
      OCR3A = duty; //sets duty (PIN5)
      TCCR3A |=  (0xC0); //non-inverting
      TCCR3A |=  (1 << WGM30);
      TCCR3B |=  (1 >> CS31);
      TCCR3B |=  (1 >> WGM32);

      OCR4A = ON; //sets duty (PIN8)
      TCCR4A |=  (0x0C); //non-inverting
      TCCR4A |=  (1 << WGM40);
      TCCR4B |=  (1 >> CS40);
      TCCR4B |=  (1 >> WGM42);

      break;
    case 4:
      //PWM output for sector 4
      // Turn on q2 and q3 (180-240 degrees),all other pins are turned off to prevent short

      TCCR4A =  0; //Clear timers
      TCCR3A =  0; //Clear timers

      //Q3 Q2
      OCR3A = duty; //sets duty (PIN5)
      TCCR3A |=  (0xC0);
      TCCR3A |=  (1 << WGM30);
      TCCR3B |=  (1 >> CS31);
      TCCR3B |=  (1 >> WGM32);

      OCR3C = ON; //sets duty (PIN3)
      TCCR3A |=  (0x08);
      TCCR3A |=  (1 << WGM30);
      TCCR3B |=  (1 >> CS31);
      TCCR3B |=  (1 >> WGM32);

      break;
    case 5:
      //PWM output for sector 5
      // Turn on q2 and q5 (240-300 degrees), all other pins are turned off to prevent short

      TCCR4A =  0; //Clear timers
      TCCR3A =  0; //Clear timers

      //Q5 Q2
      OCR4B = duty; //sets duty (PIN5)
      TCCR4A |=  (0x30); //non-inverting
      TCCR4A |=  (1 << WGM40);
      TCCR4B |=  (1 >> CS41);
      TCCR4B |=  (1 >> WGM42);

      OCR3C = ON; //sets duty (PIN3)
      TCCR3A |=  (0x08); //non-inverting
      TCCR3A |=  (1 << WGM30);
      TCCR3B |=  (1 >> CS31);
      TCCR3B |=  (1 >> WGM32);

      break;
    case 6:
      //PWM output for sector 6
      // Turn on q4 and q5 (300-360 degrees), all other pins are turned off to prevent short

      TCCR4A =  0; //Clear timers
      TCCR3A =  0; //Clear timers

      //Q5 Q4
      OCR4B = duty; //sets duty (PIN5)
      TCCR4A |=  (0x30);
      TCCR4A |=  (1 << WGM40);
      TCCR4B |=  (1 >> CS41);
      TCCR4B |=  (1 >> WGM42);

      OCR4A = ON; //sets duty (PIN3)
      TCCR4A |=  (0x80);
      TCCR4A |=  (1 << WGM40);
      TCCR4B |=  (1 >> CS41);
      TCCR4B |=  (1 >> WGM42);

      break;
  }
  // We are printing the sector and hall sensor digital readings to assure we are reading and interpreting
  // the hall effect inputs correctly.

  Serial.print("sector: ");
  Serial.print(sector);
  Serial.println();
  Serial.print("logic: ");
  Serial.print(logic);
  Serial.println();
  //  Serial.print(S1);
  //  Serial.print(", ");
  //  Serial.print(S2);
  //  Serial.print(", ");
  //  Serial.print(S3);

}
////////////////////////////////////////////////////////////////////////////////////////
// sensor_Read Function
// Updated date: 4/19/21
// This code takes the input from the hall effect readings and store them into the
// respective variables S1, S2, and S3. These variables are later used in the motor_Move
// function.
//
////////////////////////////////////////////////////////////////////////////////////////
void sensor_Read() {
  logic = (PINB >> 4) & 7;
}
/////////////////////////////////////////////////////////////////////////////////////////
// PCINT2_vect ISR
// Updated : 4/19/21
// This interrupt is set off by any change in pin 16-18, the hall effect readings, Then
// it calls the functions to update the rotor position and PWM outputs. An interrput
// assures correct timing and will assure smoother rotation of the motor.
/////////////////////////////////////////////////////////////////////////////////////////
ISR(PCINT0_vect) {
  sensor_Read();
  motor_Move();
}
